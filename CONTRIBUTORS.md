# Développeurs

* Alexis Rucar ([@Alexis_R](https://gitlab.com/Alexis_R))
* Joël Troch ([@JoelTroch](https://gitlab.com/JoelTroch))
* Julien Verkindere ([@Ilfrin](https://gitlab.com/Ilfrin))
* Julien Willaert ([@Tsunakiel](https://gitlab.com/Tsunakiel))
